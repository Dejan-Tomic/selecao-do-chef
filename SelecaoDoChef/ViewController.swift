//
//  ViewController.swift
//  Selecao do Chef
//
//  Created by Dejan Tomic on 19/06/2019.
//  Copyright © 2019 Chef's Club. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    // Os usarios
    // Mudar email para  johndoe@chefsclub.com.br e senha para 12345678
    let email = "John Doe"
    let senha = "123"
   
    // Os outlets
    @IBOutlet weak var entrarBtn: UIButton!
    @IBOutlet weak var emailCampo: UITextField!
    @IBOutlet weak var senhaCampo: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        entrarBtn.layer.cornerRadius = 10

    }

    
    //@IBAction func entrar(_ sender: Any) {
     //   if emailCampo.text == email {
      //      teste.text = "Pode entrar" }
    //    else {
      //  teste.text = "Pode entrar"
     //   }
 //   }
    
    

    
    @IBAction func emailCliqiado(_ sender: Any) {
    }
    
   
    @IBAction func senhaSegura(_ sender: Any) {
    senhaCampo.isSecureTextEntry = true
    }
    
    @IBAction func entrar(_ sender: Any) {
        //let usario = emailCampo.text
        //let senha = senhaCampo.text
        
       if emailCampo.text == email
       && senhaCampo.text == senha
             {
            let successo = UIAlertController(title: "ChefsClub", message: "Login efetuado com sucesso!", preferredStyle: .alert)
            
            successo.addAction(UIAlertAction(title: "Feito", style: .default, handler: nil))
            self.present(successo, animated: true)
        } else {
            
            let dadosErrados = UIAlertController(title: "ChefsClub", message: "Email ou senha invàlidos! Por algum acaso esqueceu sua senha?", preferredStyle: .alert)
            
            dadosErrados.addAction(UIAlertAction(title: "Tentar novamente", style: .default, handler: nil))
            self.present(dadosErrados, animated: true)
            
        }
    
    }
    

}
